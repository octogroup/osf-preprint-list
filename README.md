Use the OSF's API (https://developer.osf.io/) to gather a list of papers in MarXiv so we can gather stats.

You will need an Authorization Token (https://developer.osf.io/#tag/Authentication) to make requests.

We use the PHP Curl Class library to handle our GET requests: https://github.com/php-curl-class/php-curl-class

Put your authorization token where it says YourAuthorizationTokenHere, without the curly brackets.

Put in your preprint provider where it says PreprintProvider, without the curly brackets. For example, $curl->get('https://api.osf.io/v2/preprints/?filter[provider]=marxiv');
